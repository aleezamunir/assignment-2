# Lost In Translation

# Description
In this project you'll first see the login/startup page. You'll either login with a new user or existing user. After that you'll be redirected to the translation page. In the nav bar you'll now be able to see two buttons, translation and profile page. In the translation page you can write a phrase or a sentence in the input box, but it will not translate until you have pressed the translate button. This will add the enterede phrase/sentence to the users translations. You can use the navbar to navigate to the profile page. From here you can see the list of the users translations. You're supposed to only see the last 10 translations, but it was not implemented due to the fact that i had a lot of difficulities with this function. You can also logout from the profile page, which will log the user out and redirect to the startup page. Lastly you can clear your translation history in the profile page. 

# Link to heroku app
https://dashboard.heroku.com/apps/am-noroff-api

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts /Installation

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

