import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Profile from "./views/Profile";
import Translation from "./views/Translation";
import Startup from "./views/Startup";
import Navbar from "./components/Navbar/Navbar";

function App() {
  return (
    <BrowserRouter>
      <Navbar></Navbar>
      <div className="App">
        <Routes>
          <Route path="/" element={<Startup />} />
          <Route path="/translation" element={<Translation />} />
          <Route path="/profile" element={<Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
