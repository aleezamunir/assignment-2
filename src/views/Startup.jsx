import LoginForm from "../components/Startup/LoginForm";
import "../css/Startup.css";

const Startup = () => {
  return (
    <section id="startup">
      <h1>Lost in Translation</h1>
      <h3 id="subTitle">Let's get started</h3>
      <LoginForm />
    </section>
  );
};

export default Startup;
