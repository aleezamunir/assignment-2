import { useEffect } from "react";
import { userById } from "../api/user";
import ProfileActions from "../components/Profile/profileActions";
import ProfileHeader from "../components/Profile/profileHeader";
import TranslationHistory from "../components/Profile/translationHistory";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";
import "../css/Profile.css";

const Profile = () => {
  const { user, setUser } = useUser();

  useEffect(() => {
    const findUser = async () => {
      const [error, latestUser] = await userById(user.id);
      if (error === null) {
        storageSave("translation-user", latestUser);
        setUser(latestUser);
      }
    };

    //findUser();
  }, [setUser, user.id]);

  return (
    <section id="profile">
      <h1>Profile</h1>
      <ProfileHeader username={user.username} />
      <TranslationHistory translations={user.translations} />
      <ProfileActions />
    </section>
  );
};

export default withAuth(Profile);
