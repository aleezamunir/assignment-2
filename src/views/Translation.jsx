import { addTranslation } from "../api/translation";
import TranslationForm from "../components/Translation/TranslationForm";
import TranslationText from "../components/Translation/TranslationText";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";
import "../css/Translation.css";

const Translation = () => {
  const { user, setUser } = useUser();

  //A function that updates the users translation array with the translation that was just entered by the user
  const handleTranslationClick = async (translation) => {
    if (!translation) {
      alert("Write something first");
      return;
    }

    const [error, updatedUser] = await addTranslation(user, translation.trim());
    if (error !== null) {
      return;
    }

    storageSave("translation-user", updatedUser);
    setUser(updatedUser);

    console.log("Error", error);
    console.log("Updated user", updatedUser);
  };

  return (
    <section>
      <section id="translate">
        <h1>Translation</h1>
        <TranslationForm
          onTranslation={handleTranslationClick}
        ></TranslationForm>
      </section>
      <section id="images">
        <h1>The Direct Translation</h1>
        {/* If the user translations array is not empty, the translation will be set to the last translation in list */}
        {user.translations.length && (
          <TranslationText
            sentence={user.translations[user.translations.length - 1]}
          />
        )}
        {/* If the use translations array is empty, the translation will be set to 'Hello' */}
        {!user.translations.length && <TranslationText sentence={"hello"} />}
      </section>
    </section>
  );
};

export default withAuth(Translation);
