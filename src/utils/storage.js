//Saves to storage
export const storageSave = (key, value) => {
  if (!key) {
    throw new Error("storageSave: No storage key provided");
  }

  if (!value) {
    throw new Error("storageSave: No value provided");
  }
  sessionStorage.setItem(key, JSON.stringify(value));
};
//Reades from storage
export const storageRead = (key) => {
  const data = sessionStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }

  return null;
};
//Deletes from storage
export const storageDelete = (key) => {
  sessionStorage.removeItem(key);
};
