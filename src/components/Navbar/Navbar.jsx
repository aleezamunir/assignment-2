import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import "../../css/Navbar.css";

const Navbar = () => {
  const { user } = useUser();

  return (
    <nav>
      <h3>Lost in Translation</h3>
      {user !== null && (
        <>
          <button>
            <NavLink to="/translation">Translation</NavLink>
          </button>
          <button>
            <NavLink to="/profile">Profile Page</NavLink>
          </button>
        </>
      )}
    </nav>
  );
};
export default Navbar;
