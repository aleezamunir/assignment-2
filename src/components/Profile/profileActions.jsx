import { clearOrderHistory } from "../../api/translation";
import { useUser } from "../../context/UserContext";
import { storageDelete, storageSave } from "../../utils/storage";

const ProfileActions = () => {
  const { user, setUser } = useUser();
  //Event that logs the user out
  const handleLogoutClick = () => {
    if (window.confirm("Do you want to logout?")) {
      storageDelete("translation-user");
      setUser(null);
    }
  };
  //Event that handles the clear history button
  const handleClearHistoryClick = async () => {
    if (
      !window.confirm("Do you want to clear history?\nThis can not be undone")
    ) {
      return;
    }

    const [clearErrorMessage] = await clearOrderHistory(user.id);

    if (clearErrorMessage !== null) {
      return;
    }

    const updatedUser = {
      ...user,
      translations: [],
    };

    storageSave("translation-user", updatedUser);
    setUser(updatedUser);
  };

  return (
    <>
      <button onClick={handleClearHistoryClick}>Clear history</button>
      <button onClick={handleLogoutClick}>Logout</button>
    </>
  );
};

export default ProfileActions;
