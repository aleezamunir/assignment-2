import TranslationHistorySentence from "./TranslationHistorySentence";

const TranslationHistory = ({ translations }) => {
  //Lists all of the translations in the translation list
  const translationList = translations?.map((sentence, index) => (
    <TranslationHistorySentence
      key={index + "-" + sentence}
      sentence={sentence}
    />
  ));
  return <ul>{translationList}</ul>;
};

export default TranslationHistory;
