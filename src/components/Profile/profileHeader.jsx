const ProfileHeader = ({ username }) => {
  return (
    <header>
      <h4>Last 10 translations for {username}</h4>
    </header>
  );
};

export default ProfileHeader;
