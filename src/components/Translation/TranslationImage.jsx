const TranslationImage = ({ letter }) => {
  const ThisLetter = "/img/individial_signs/" + letter + ".png";
  return <img src={ThisLetter}></img>;
};

export default TranslationImage;
