import { useForm } from "react-hook-form";
const TranslationForm = ({ onTranslation }) => {
  const { register, handleSubmit } = useForm();

  const onSubmit = ({ translation }) => {
    onTranslation(translation);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <input
            id="newTranslation"
            type="text"
            {...register("translation")}
            placeholder="Hello"
          ></input>
          <button type="submit">Translate</button>
        </fieldset>
      </form>
    </>
  );
};

export default TranslationForm;
