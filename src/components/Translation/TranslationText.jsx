import TranslationImage from "./TranslationImage";

const TranslationText = ({ sentence }) => {
  const sentenceList = Array.from(sentence);

  console.log(sentenceList);

  return (
    <fieldset>
      {sentenceList.map((item, index) => (
        <TranslationImage key={index} letter={item}></TranslationImage>
      ))}
    </fieldset>
  );
};

export default TranslationText;
